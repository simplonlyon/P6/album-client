import { PictureService } from "./service/picture-service";
import * as $ from 'jquery';
import { LoginService } from "./service/login-service";

let loginService = new LoginService();

let service = new PictureService();

showPictures(service);
showLogin();

$('#toggle-add').on('click', () => $('#add-popup').toggle());
$('#logout').on('click', () => {
    loginService.logout();
    showLogin();
});

$('#add-form').on('submit', function (event) {
    event.preventDefault();
    let errors = $('#errors').empty();
    let data = new FormData(<HTMLFormElement>this);
    service.add(data)
        .then(picture => showPictures(service))
        .catch(err => {
            showLogin();
            errors.text('There has been a problem')
        });

});

$('#login-form').on('submit', function(event) {
    event.preventDefault();
    let errors = $('#errors').empty();
    let username = <string>$('#username').val();
    let password = <string>$('#password').val();
    loginService.login(username, password)
    .then( () => showLogin())
    .catch(err => errors.text('Authentication error'));
    
});

function showLogin() {
    if(!loginService.getToken()) {
        $('#login-form').show();
        $('#add-form').hide();
        $('#logout').hide();
    } else {
        $('#login-form').hide();
        $('#add-form').show();
        $('#logout').show();
    }
}


function showPictures(service) {
    $('#album').empty();
    service.findAll().then(data => {

        for (const picture of data) {
            let article = $('<article></article>');
            let img = $('<img>');
            let p = $('<p></p>');

            img.attr('src', picture.url);
            p.text(picture.description);

            article.append(img, p);

            $('#album').append(article);
        }

    });
}