import {ajax} from 'jquery';
import { Picture } from '../entities/picture';
import { LoginService } from './login-service';

export class PictureService {
    private apiUrl = 'http://localhost:8080/api/picture/';
    private loginService = new LoginService();

    findAll():Promise<Picture[]> {
        return ajax(this.apiUrl);
    }

    add(picture:any):Promise<Picture> {
        return ajax(this.apiUrl, {
            headers: {
                'Authorization': 'Bearer '+this.loginService.getToken()
            },
            data: picture,
            method: 'POST',
            processData: false,
            contentType: false 
        }).catch(err => {
            if(err.status === 401) {
                this.loginService.logout();
            }
            throw err;
        });
    }
    

}